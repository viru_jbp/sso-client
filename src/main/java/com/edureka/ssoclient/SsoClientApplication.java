package com.edureka.ssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.logging.Logger;

/**
 * The spring security framework defines a class named CommonOAuth2Provider.
 * This class partially defines the ClientRegistration instances for the most common providers you can use
 * for authentication, which include:
 *
 * Google
 * GitHub
 * Facebook
 * Okta
 */
@SpringBootApplication
@Controller
public class SsoClientApplication extends WebSecurityConfigurerAdapter {
	private Logger logger = Logger.getLogger(SsoClientApplication.class.getName());

	@GetMapping("/main")
	public String main() {
		return "main.html";
	}

	/**star
	 * The implementation of the Authentication object used by the framework is named OAuth2AuthenticationToken.
	 * Spring boot automatically injects Authentication object representing the user in the method's parameter
	 *
	 */
	@GetMapping("/home")
	public String main(OAuth2AuthenticationToken token) {
		logger.info("********");
		logger.info(String.valueOf(token.getPrincipal()));

		return "home.html";
	}

	/**
	 * http.oauth2Login() adds a new authentication filter(OAuth2LoginAuthenticationFilter) to the filter chain
	 * @par am http
	 * @throws Exception
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.oauth2Login();
		// Below uses a Customizer to set the ClientRegistrationRepository instance.
		// it's an alternative to using clientRepository as @Bean.
		// One configuration option is as good as the other, but remember To keep your code easy to understand,
		// avoid mixing configuration approaches.
		// Either use an approach where you set everything with beans in the context
		// or use the code inline configuration style.
		/*http.oauth2Login(new Customizer<OAuth2LoginConfigurer<HttpSecurity>>() {
			@Override
			public void customize(OAuth2LoginConfigurer<HttpSecurity> httpSecurityOAuth2LoginConfigurer) {
				httpSecurityOAuth2LoginConfigurer.clientRegistrationRepository(clientRepository());
			}
		});*/
		http.authorizeRequests()
				.anyRequest()
				.authenticated();
	}

	/**
	 * Authorization URI--The URI to which the client redirects the user for authentication
	 * Token URI--The URI that the client calls to obtain an access token and a refresh token,
	 * User info URI--The URI that the client can call after obtaining an access token to get more details about the user
	 * The values are taken from : https://developer.github.com/apps/building-oauth-apps/authorizing-oauth-apps/
	 *
	 * Using the values from the CommonOAuth2Provider class also means that you rely on the fact that
	 * the provider you use won’t change the URLs and the other relevant values.
	 * While this is not likely, if you want to avoid this situation, the option is to implement
	 * ClientRegistration as presented below. This enables you to configure the URLs and related provider values
	 * in a configuration file.
	 *
	 * The client ID and client secret are credentials, which makes them sensitive data.
	 * In a real-world application, they should be obtained from a secrets vault,
	 * and you should never directly write credentials in the source code.
	 * @return
	 */
	private ClientRegistration clientRegistration() {
		// If your authorization server is not among the common providers,
		// then you have no other option but to define ClientRegistration entirely as presented.
		/*return ClientRegistration.withRegistrationId("github")
					.clientId("0c20ffd7f555b7075622")
					.clientSecret("852577d648accd412bb733fef57037fa3087172a")
					.scope(new String[] {"read:user"})

					.authorizationUri("https://github.com/login/oauth/authorize")
					.tokenUri("https://github.com/login/oauth/access_token")
					.userInfoUri("https://api.github.com/user")
					.userNameAttributeName("id")
					.clientName("GitHub")
					.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
					.redirectUriTemplate("{baseUrl}/login/oauth2/code/{registrationId}")
					.build();*/

		// If your authorization server is among the common providers eg Google, Github, Facebook, Okta,
		return CommonOAuth2Provider.GITHUB
				.getBuilder("github")
				.clientId("0c20ffd7f555b7075622")
				//.clientId("60d40eefb4b844bbbd51")
				//.clientSecret("852577d648accd412bb733fef57037fa3087172a")
				.clientSecret("e8ef9ba50992388a85687d47c8294812ede34e75")
				//.clientSecret("99c8c0d973c5aaf2f9759e0a952541dc55dbd960")
				.build();

	}

	/**
	 * As an alternative to this way of registering ClientRegistrationRepository,
	 * you can use a Customizer object as a parameter of the oauth2Login() method of the HttpSecurity object.
	 * @return
	 */
	@Bean
	public ClientRegistrationRepository clientRepository() {
		ClientRegistration registration = clientRegistration();

		return new InMemoryClientRegistrationRepository(registration);
	}


	public static void main(String[] args) {
		SpringApplication.run(SsoClientApplication.class, args);
	}


}
